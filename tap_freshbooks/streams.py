"""Stream type classes for tap-freshbooks."""

from typing import Optional

from singer_sdk import typing as th  # JSON Schema typing helpers

from tap_freshbooks.client import freshbooksStream


class BusinessStream(freshbooksStream):
    name = "business"
    path = "/auth/api/v1/users/me"
    records_jsonpath = "$.response.business_memberships[*]"

    schema = th.PropertiesList(
        th.Property("id", th.IntegerType),
        th.Property("role", th.StringType),
        th.Property("unacknowledged_change", th.BooleanType),
        th.Property("fasttrack_token", th.StringType),
        th.Property("business", th.ObjectType(
            th.Property("id", th.IntegerType),
            th.Property("account_id", th.StringType),
            th.Property("date_format", th.StringType),
            th.Property("active", th.BooleanType),
            th.Property("timezone", th.StringType),
            th.Property("status", th.StringType),
            th.Property("advanced_accounting_enabled", th.BooleanType)
            ))
    ).to_dict()

    def get_child_context(self, record: dict, context: Optional[dict]) -> dict:
        """Return a context dictionary for child streams."""
        return {
            "business_id": record["business"]["id"],
            "accounting_systemid": record["business"]["account_id"],
        }


class JournalEntriesStream(freshbooksStream):
    name = "journal_entries"
    path = "/accounting/account/{accounting_systemid}/journal_entries/journal_entry_details"
    records_jsonpath = "$.response.result.journal_entry_details[*]"

    primary_keys = ["id"]
    replication_key = None
    parent_stream_type = BusinessStream

    schema = th.PropertiesList(
        th.Property(
            "account",
            th.ObjectType(
                th.Property("account_name", th.StringType),
                th.Property("account_number", th.StringType),
                th.Property("account_type", th.StringType),
                th.Property("accountid", th.IntegerType),
                th.Property("accounting_systemid", th.StringType),
                th.Property("id", th.IntegerType),
            ),
        ),
        th.Property("accounting_systemid", th.StringType),
        th.Property(
            "balance",
            th.ObjectType(
                th.Property("amount", th.StringType),
                th.Property("code", th.StringType),
            ),
        ),
        th.Property(
            "credit",
            th.ObjectType(
                th.Property("amount", th.StringType),
                th.Property("code", th.StringType),
            ),
        ),
        th.Property(
            "debit",
            th.ObjectType(
                th.Property("amount", th.StringType),
                th.Property("code", th.StringType),
            ),
        ),
        th.Property("description", th.StringType),
        th.Property("detail_type", th.StringType),
        th.Property("detailid", th.IntegerType),
        th.Property(
            "entry",
            th.ObjectType(
                th.Property("accounting_systemid", th.StringType),
                th.Property("categoryid", th.IntegerType),
                th.Property("clientid", th.StringType),
                th.Property("creditid", th.StringType),
                th.Property("entryid", th.IntegerType),
                th.Property("expenseid", th.IntegerType),
                th.Property("id", th.IntegerType),
                th.Property("incomeid", th.StringType),
                th.Property("invoiceid", th.StringType),
                th.Property("paymentid", th.StringType),
            ),
        ),
        th.Property("id", th.IntegerType),
        th.Property("name", th.StringType),
        th.Property(
            "sub_account",
            th.ObjectType(
                th.Property("account_sub_name", th.StringType),
                th.Property("account_sub_number", th.StringType),
                th.Property("accounting_systemid", th.StringType),
                th.Property("id", th.IntegerType),
                th.Property("parentid", th.IntegerType),
                th.Property("sub_accountid", th.IntegerType),
            ),
        ),
        th.Property("user_entered_date", th.StringType),
    ).to_dict()


class ProjectsStream(freshbooksStream):
    name = "projects"
    path = "/projects/business/{business_id}/projects"
    records_jsonpath = "$.projects[*]"

    primary_keys = ["id"]
    replication_key = None
    parent_stream_type = BusinessStream

    schema = th.PropertiesList(
        th.Property("id", th.IntegerType),
        th.Property("title", th.StringType),
        th.Property("description", th.StringType),
        th.Property("due_date", th.StringType),
        th.Property("client_id", th.IntegerType),
        th.Property("internal", th.BooleanType),
        th.Property("budget", th.IntegerType),
        th.Property("fixed_price", th.StringType),
        th.Property("rate", th.StringType),
        th.Property("billing_method", th.StringType),
        th.Property("project_type", th.StringType),
        th.Property("project_manager_id", th.StringType),
        th.Property("active", th.BooleanType),
        th.Property("complete", th.BooleanType),
        th.Property("sample", th.BooleanType),
        th.Property("created_at", th.StringType),
        th.Property("updated_at", th.StringType),
        th.Property("logged_duration", th.StringType),
        th.Property(
            "services",
            th.ArrayType(
                th.ObjectType(
                    th.Property("business_id", th.IntegerType),
                    th.Property("id", th.IntegerType),
                    th.Property("name", th.StringType),
                    th.Property("billable", th.BooleanType),
                    th.Property("vis_state", th.IntegerType),
                )
            ),
        ),
        th.Property("billed_amount", th.StringType),
        th.Property("billed_status", th.StringType),
        th.Property("retainer_id", th.StringType),
        th.Property("expense_markup", th.StringType),
        th.Property("service_estimate_type", th.StringType),
        th.Property("group_id", th.IntegerType),
    ).to_dict()


class BillsStream(freshbooksStream):
    name = "bills"
    path = "/accounting/account/{accounting_systemid}/bills/bills"
    records_jsonpath = "$.response.result.bills[*]"

    primary_keys = ["id"]
    replication_key = None
    parent_stream_type = BusinessStream

    bill_line_schema = th.ObjectType(
        th.Property(
            "amount",
            th.ObjectType(
                th.Property("amount", th.StringType),
                th.Property("code", th.StringType),
            ),
        ),
        th.Property(
            "category",
            th.ObjectType(
                th.Property("category", th.StringType),
                th.Property("categoryid", th.IntegerType),
                th.Property("created_at", th.StringType),
                th.Property("id", th.IntegerType),
                th.Property("is_cogs", th.BooleanType),
                th.Property("is_editable", th.BooleanType),
                th.Property("parentid", th.IntegerType),
                th.Property("transaction_posted", th.BooleanType),
                th.Property("updated_at", th.StringType),
                th.Property("vis_state", th.IntegerType),
            ),
        ),
        th.Property("description", th.StringType),
        th.Property("id", th.IntegerType),
        th.Property("list_index", th.IntegerType),
        th.Property("quantity", th.StringType),
        th.Property("tax_amount1", th.StringType),
        th.Property("tax_amount2", th.StringType),
        th.Property("tax_authorityid1", th.StringType),
        th.Property("tax_authorityid2", th.StringType),
        th.Property("tax_name1", th.StringType),
        th.Property("tax_name2", th.StringType),
        th.Property("tax_percent1", th.StringType),
        th.Property("tax_percent2", th.StringType),
        th.Property(
            "total_amount",
            th.ObjectType(
                th.Property("amount", th.StringType),
                th.Property("code", th.StringType),
            ),
        ),
        th.Property(
            "unit_cost",
            th.ObjectType(
                th.Property("amount", th.StringType),
                th.Property("code", th.StringType),
            ),
        ),
    )

    schema = th.PropertiesList(
        th.Property(
            "amount",
            th.ObjectType(
                th.Property("amount", th.StringType),
                th.Property("code", th.StringType),
            ),
        ),
        th.Property("attachment", th.StringType),
        th.Property("bill_number", th.StringType),
        th.Property("bill_payments", th.CustomType({"type": ["array", "string"]})),
        th.Property("created_at", th.StringType),
        th.Property("currency_code", th.StringType),
        th.Property("due_date", th.StringType),
        th.Property("due_offset_days", th.IntegerType),
        th.Property("id", th.IntegerType),
        th.Property("issue_date", th.StringType),
        th.Property("language", th.StringType),
        th.Property("lines", th.ArrayType(bill_line_schema)),
        th.Property(
            "outstanding",
            th.ObjectType(
                th.Property("amount", th.StringType),
                th.Property("code", th.StringType),
            ),
        ),
        th.Property("overall_category", th.StringType),
        th.Property("overall_description", th.StringType),
        th.Property(
            "paid",
            th.ObjectType(
                th.Property("amount", th.StringType),
                th.Property("code", th.StringType),
            ),
        ),
        th.Property("status", th.StringType),
        th.Property(
            "tax_amount",
            th.ObjectType(
                th.Property("amount", th.StringType),
                th.Property("code", th.StringType),
            ),
        ),
        th.Property(
            "total_amount",
            th.ObjectType(
                th.Property("amount", th.StringType),
                th.Property("code", th.StringType),
            ),
        ),
        th.Property("updated_at", th.StringType),
        th.Property("vis_state", th.IntegerType),
    ).to_dict()


class VendorsStream(freshbooksStream):
    name = "vendors"
    path = "/accounting/account/{accounting_systemid}/bill_vendors/bill_vendors"
    records_jsonpath = "$.response.result.bill_vendors[*]"

    primary_keys = ["vendorid"]
    replication_key = "updated_at"
    parent_stream_type = BusinessStream

    schema = th.PropertiesList(
        th.Property("account_number", th.StringType),
        th.Property("city", th.StringType),
        th.Property("country", th.StringType),
        th.Property("created_at", th.StringType),
        th.Property("currency_code", th.StringType),
        th.Property("is_1099", th.BooleanType),
        th.Property("language", th.StringType),
        th.Property("note", th.StringType),
        th.Property(
            "outstanding_balance", th.CustomType({"type": ["array", "string"]})
        ),
        th.Property("overdue_balance", th.CustomType({"type": ["array", "string"]})),
        th.Property("phone", th.StringType),
        th.Property("postal_code", th.StringType),
        th.Property("primary_contact_email", th.StringType),
        th.Property("primary_contact_first_name", th.StringType),
        th.Property("primary_contact_last_name", th.StringType),
        th.Property("province", th.StringType),
        th.Property("street", th.StringType),
        th.Property("street2", th.StringType),
        th.Property("tax_defaults", th.CustomType({"type": ["array", "string"]})),
        th.Property("updated_at", th.StringType),
        th.Property("vendor_name", th.StringType),
        th.Property("vendorid", th.IntegerType),
        th.Property("vis_state", th.IntegerType),
        th.Property("website", th.StringType),
    ).to_dict()


class ExpensesStream(freshbooksStream):
    name = "expenses"
    path = "/accounting/account/{accounting_systemid}/expenses/expenses"
    records_jsonpath: str = "$.response.result.expenses[*]"

    primary_keys = ["id"]
    replication_key = "updated"
    parent_stream_type = BusinessStream

    schema = th.PropertiesList(
        th.Property("account_name", th.StringType),
        th.Property("accountid", th.StringType),
        th.Property("accounting_systemid", th.StringType),
        th.Property(
            "amount",
            th.ObjectType(
                th.Property("amount", th.StringType),
                th.Property("code", th.StringType),
            ),
        ),
        th.Property("background_jobid", th.StringType),
        th.Property("bank_name", th.StringType),
        th.Property("bill_matches", th.CustomType({"type": ["array", "string"]})),
        th.Property("billable", th.BooleanType),
        th.Property("categoryid", th.IntegerType),
        th.Property("clientid", th.IntegerType),
        th.Property("compounded_tax", th.BooleanType),
        th.Property("converse_projectid", th.StringType),
        th.Property("date", th.StringType),
        th.Property("expenseid", th.IntegerType),
        th.Property("ext_accountid", th.StringType),
        th.Property("ext_invoiceid", th.IntegerType),
        th.Property("ext_systemid", th.IntegerType),
        th.Property("from_bulk_import", th.BooleanType),
        th.Property("has_receipt", th.BooleanType),
        th.Property("id", th.IntegerType),
        th.Property("include_receipt", th.BooleanType),
        th.Property("invoiceid", th.StringType),
        th.Property("is_cogs", th.BooleanType),
        th.Property("isduplicate", th.BooleanType),
        th.Property("markup_percent", th.StringType),
        th.Property("modern_projectid", th.StringType),
        th.Property("notes", th.StringType),
        th.Property("potential_bill_payment", th.BooleanType),
        th.Property("profileid", th.StringType),
        th.Property("projectid", th.IntegerType),
        th.Property("staffid", th.IntegerType),
        th.Property("status", th.IntegerType),
        th.Property("taxAmount1", th.StringType),
        th.Property("taxAmount2", th.StringType),
        th.Property("taxName1", th.StringType),
        th.Property("taxName2", th.StringType),
        th.Property("taxPercent1", th.StringType),
        th.Property("taxPercent2", th.StringType),
        th.Property("transactionid", th.StringType),
        th.Property("updated", th.StringType),
        th.Property("vendor", th.StringType),
        th.Property("version", th.StringType),
        th.Property("vis_state", th.IntegerType),
    ).to_dict()


class ClientsStream(freshbooksStream):
    name = "clients"
    path = "/accounting/account/{accounting_systemid}/users/clients"
    records_jsonpath = "$.response.result.clients[*]"

    primary_keys = ["id"]
    replication_key = None
    parent_stream_type = BusinessStream

    schema = th.PropertiesList(
        th.Property("accounting_systemid", th.StringType),
        th.Property("allow_email_include_pdf", th.BooleanType),
        th.Property("allow_late_fees", th.BooleanType),
        th.Property("allow_late_notifications", th.BooleanType),
        th.Property("bus_phone", th.StringType),
        th.Property("company_industry", th.StringType),
        th.Property("company_size", th.IntegerType),
        th.Property("currency_code", th.StringType),
        th.Property("direct_link_token", th.StringType),
        th.Property("email", th.StringType),
        th.Property("exceeds_client_limit", th.IntegerType),
        th.Property("fax", th.StringType),
        th.Property("fname", th.StringType),
        th.Property("has_retainer", th.BooleanType),
        th.Property("home_phone", th.StringType),
        th.Property("id", th.IntegerType),
        th.Property("language", th.StringType),
        th.Property("last_activity", th.StringType),
        th.Property("last_login", th.StringType),
        th.Property("level", th.IntegerType),
        th.Property("lname", th.StringType),
        th.Property("mob_phone", th.StringType),
        th.Property("note", th.StringType),
        th.Property("notified", th.BooleanType),
        th.Property("num_logins", th.IntegerType),
        th.Property("organization", th.StringType),
        th.Property("p_city", th.StringType),
        th.Property("p_code", th.StringType),
        th.Property("p_country", th.StringType),
        th.Property("p_province", th.StringType),
        th.Property("p_street", th.StringType),
        th.Property("p_street2", th.StringType),
        th.Property("pref_email", th.BooleanType),
        th.Property("pref_gmail", th.BooleanType),
        th.Property("retainer_id", th.StringType),
        th.Property("role", th.StringType),
        th.Property("s_city", th.StringType),
        th.Property("s_code", th.StringType),
        th.Property("s_country", th.StringType),
        th.Property("s_province", th.StringType),
        th.Property("s_street", th.StringType),
        th.Property("s_street2", th.StringType),
        th.Property("signup_date", th.StringType),
        th.Property("statement_token", th.StringType),
        th.Property("subdomain", th.StringType),
        th.Property("updated", th.StringType),
        th.Property("userid", th.IntegerType),
        th.Property("username", th.StringType),
        th.Property("uuid", th.StringType),
        th.Property("vat_name", th.StringType),
        th.Property("vat_number", th.StringType),
        th.Property("vis_state", th.IntegerType),
    ).to_dict()


class InvoicesStream(freshbooksStream):
    name = "invoices"
    path = "/accounting/account/{accounting_systemid}/invoices/invoices"
    records_jsonpath = "$.response.result.invoices[*]"

    primary_keys = ["uuid"]
    replication_key = "updated"
    parent_stream_type = BusinessStream
    schema = th.PropertiesList(
        th.Property("accountid", th.StringType),
        th.Property("accounting_systemid", th.StringType),
        th.Property("address", th.StringType),
        th.Property(
            "amount",
            th.ObjectType(
                th.Property("amount", th.StringType), th.Property("code", th.StringType)
            ),
        ),
        th.Property("auto_bill", th.BooleanType),
        th.Property("autobill_status", th.StringType),
        th.Property("basecampid", th.IntegerType),
        th.Property("city", th.StringType),
        th.Property("code", th.StringType),
        th.Property("country", th.StringType),
        th.Property("create_date", th.StringType),
        th.Property("created_at", th.StringType),
        th.Property("currency_code", th.StringType),
        th.Property("current_organization", th.StringType),
        th.Property("customerid", th.IntegerType),
        th.Property("date_paid", th.StringType),
        th.Property("deposit_amount", th.NumberType),
        th.Property("deposit_percetage", th.NumberType),
        th.Property("deposit_status", th.StringType),
        th.Property("description", th.StringType),
        th.Property("discount_description", th.StringType),
        th.Property(
            "discount_total",
            th.ObjectType(
                th.Property("amount", th.StringType),
                th.Property("code", th.StringType),
            ),
        ),
        th.Property("discount_value", th.StringType),
        th.Property("display_status", th.StringType),
        th.Property("dispute_status", th.StringType),
        th.Property("due_date", th.StringType),
        th.Property("due_offset_days", th.IntegerType),
        th.Property("estimateid", th.IntegerType),
        th.Property("ext_archive", th.IntegerType),
        th.Property("fname", th.StringType),
        th.Property("fulfillment_date", th.StringType),
        th.Property("generation_data", th.StringType),
        th.Property("gmail", th.BooleanType),
        th.Property("id", th.IntegerType),
        th.Property("invoice_number", th.StringType),
        th.Property("invoiceid", th.IntegerType),
        th.Property("language", th.StringType),
        th.Property("last_order_status", th.StringType),
        th.Property("lname", th.StringType),
        th.Property(
            "net_paid_amount",
            th.ObjectType(
                th.Property("amount", th.StringType),
                th.Property("code", th.StringType),
            ),
        ),
        th.Property("notes", th.StringType),
        th.Property("organization", th.StringType),
        th.Property(
            "outstanding",
            th.ObjectType(
                th.Property("amount", th.StringType),
                th.Property("code", th.StringType),
            ),
        ),
        th.Property(
            "paid",
            th.ObjectType(
                th.Property("amount", th.StringType),
                th.Property("code", th.StringType),
            ),
        ),
        th.Property("ownerid", th.IntegerType),
        th.Property("parent", th.NumberType),
        th.Property("payment_details", th.StringType),
        th.Property("payment_status", th.StringType),
        th.Property("po_number", th.StringType),
        th.Property("province", th.StringType),
        th.Property("return_uri", th.StringType),
        th.Property("sentid", th.IntegerType),
        th.Property("show_attachments", th.BooleanType),
        th.Property("status", th.IntegerType),
        th.Property("street", th.StringType),
        th.Property("street2", th.StringType),
        th.Property("template", th.StringType),
        th.Property("terms", th.StringType),
        th.Property("updated", th.StringType),
        th.Property("uuid", th.StringType),
        th.Property("v3_status", th.StringType),
        th.Property("vat_name", th.StringType),
        th.Property("vat_number", th.StringType),
        th.Property("version", th.StringType),
        th.Property("vis_state", th.IntegerType),
    ).to_dict()
