"""REST client handling, including freshbooksStream base class."""

import requests
from pathlib import Path
from typing import Any, Dict, Optional

from memoization import cached
from pendulum import parse
from singer_sdk.streams import RESTStream

from tap_freshbooks.auth import OAuth2Authenticator


SCHEMAS_DIR = Path(__file__).parent / Path("./schemas")


class freshbooksStream(RESTStream):
    """freshbooks stream class."""

    url_base = "https://api.freshbooks.com"

    records_jsonpath = "$[*]"  # Or override `parse_response`.

    @cached
    def get_starting_time(self, context):
        if self.config.get("start_date"):
            start_date = parse(self.config["start_date"])

        else:
            start_date = None

        
        rep_key = self.get_starting_replication_key_value(context)
        if rep_key:
            rep_key = parse(rep_key)
        return rep_key or start_date

    @property
    @cached
    def authenticator(self) -> OAuth2Authenticator:
        """Return a new authenticator object."""
        return OAuth2Authenticator(
            self, self._tap.config, "https://api.freshbooks.com/auth/oauth/token"
        )

    @property
    def http_headers(self) -> dict:
        """Return the http headers needed."""
        headers = {}
        if "user_agent" in self.config:
            headers["User-Agent"] = self.config.get("user_agent")
            headers["Authorization"] = f"Bearer {self.config['access_token']}"
        return headers

    def get_next_page_token(
        self, response: requests.Response, previous_token: Optional[Any]
    ) -> Optional[Any]:
        """Return a token for identifying next page or None if no more pages."""

        # if current page * 100 > total
        r = response.json()
        try:
            total_pages = r["response"]["result"]["total"]
            current_page = r["response"]["result"]["page"]
        except KeyError as ke:
            return None
        if current_page >= total_pages:
            return None

        return current_page + 1

    def get_url_params(
        self, context: Optional[dict], next_page_token: Optional[Any]
    ) -> Dict[str, Any]:
        """Return a dictionary of values to be used in URL parameterization."""
        params: dict = {}
        params["per_page"] = 100  # max value
        if next_page_token:
            params["page"] = next_page_token
        rep_key_value = self.get_starting_time(context)
        if rep_key_value is not None:
            params["search[date_min]"] = str(rep_key_value)[:10]

        return params
