"""freshbooks tap class."""

from typing import List

from singer_sdk import Tap, Stream
from singer_sdk import typing as th  # JSON schema typing helpers

from tap_freshbooks.streams import (
    ClientsStream,
    JournalEntriesStream,
    ProjectsStream,
    BillsStream,
    VendorsStream,
    ExpensesStream,
    InvoicesStream,
    BusinessStream
)

STREAM_TYPES = [
    BusinessStream,
    ClientsStream,
    JournalEntriesStream,
    ProjectsStream,
    BillsStream,
    VendorsStream,
    ExpensesStream,
    InvoicesStream,
]


class Tapfreshbooks(Tap):
    """freshbooks tap class."""

    name = "tap-freshbooks"

    def __init__(
        self,
        config=None,
        catalog=None,
        state=None,
        parse_env_config=False,
        validate_config=True,
    ) -> None:
        self.config_file = config[0]
        super().__init__(config, catalog, state, parse_env_config, validate_config)

    config_jsonschema = th.PropertiesList(
        th.Property(
            "access_token",
            th.StringType,
            required=True,
            secret=True,  # Flag config as protected.
            description="The token to authenticate against the API service",
        ),
        th.Property(
            "refresh_token",
            th.StringType,
            required=True,
            secret=True,
        ),
        th.Property(
            "client_id",
            th.StringType,
            required=True,
            secret=True,
        ),
        th.Property(
            "secret_id",
            th.StringType,
            required=True,
            secret=True,
        ),
        th.Property(
            "start_date",
            th.DateTimeType,
            description="The earliest record date to sync",
        ),
        th.Property(
            "redirect_uri",
            th.DateTimeType,
            required=True,
        ),
    ).to_dict()

    def discover_streams(self) -> List[Stream]:
        """Return a list of discovered streams."""
        return [stream_class(tap=self) for stream_class in STREAM_TYPES]


if __name__ == "__main__":
    Tapfreshbooks.cli()
